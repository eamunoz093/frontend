<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Avanzar company</title>
    <link rel="icon" href="../recursos/avanzar.ico">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css-Avanzar/normalize.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/Fondo.css">
    <link rel="stylesheet" href="css/efecto_hover.css">
    <link rel="stylesheet" href="css/Nav_Superior y footer.css">
    <script src="../assets/js/jquery.js"></script>

</head>


<body>

    <div class="container-fluid">

        <div class="row">

            <!--Menu lateral izquierdo-->
            <div class="col-12 col-lg-3">
                <div style="width: 20%;" class="ms-5 list-group fixed-top">

                    <a id="fondos" href="../Modulo Registro Usuario/Vista_Usuario.html"
                        class="py-4 list-group-item list-group-item-action text-center fs-5 "><i
                            class="bi bi-person">Usuario </i>
                    </a>

                    <a id="fondos" href="../Modulo registro de cliente/Vista_Cliente.html"
                        class="py-4 list-group-item list-group-item-action text-center fs-5">
                        <i class="bi bi-people-fill"> Cliente</i></a>

                    <a id="fondos" href="../Modulo Vista Eps/Vista_Eps.html"
                        class="py-4 list-group-item list-group-item-action text-center fs-5">EPS</a>

                    <a id="fondos" href="../Modulo Vista Arl/Vista_Arl.html"
                        class="py-4 list-group-item list-group-item-action text-center fs-5">ARL</a>

                    <a id="fondos" href="../Modulo Vista Afp/Vista_Afp.html"
                        class="py-4 list-group-item list-group-item-action text-center fs-5">AFP</a>

                    <a id="fondos" href="../Modulo Vista Caja de compensacion/Vista_Caja_de_compensacion.html"
                        class="py-4 list-group-item list-group-item-action text-center fs-5">Caja de
                        compensacion</a>
                </div>
            </div>
            <!--Final del menu lateral-->

            <!--barra derecha superior-->

            <!--cuadro superior-->
            <div style="width: 900px; margin-left: 29%;" class="navegador col-12 bg-info shadow-sm fixed-top">
                <nav class="navbar navbar-expand-lg navbar-light ">
                    <div class="container-fluid ">
                        <ul class="navbar-nav me-auto mb-2 my-3">
                            <li class="nav-item ">
                                <a class="navbar-text text-white"><b>Bienvenido</b></a>
                            </li>
                            <li id="tipo_de_usuario" class="nav-item">
                                <a class="navbar-text text-primary"><b>Administrador</b></a>
                            </li>

                            <li class="nav-item">
                                <a href="../index.html">
                                    <button type="button" class=" boton btn-primary">Exit</button>
                                </a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
            <!--Final del navegador superior-->

            <!--Contenido Registro de usuario-->

            <div style="position: absolute; top: 18%; left: 29%;  width: 900px;" class="col-12">

                <form class="row col-12" name="formData">
                    <div class="row col-12">

                        <?php
                        if(isset($_GET["id_Usuario"])){
                        ?>
                                    <input id="id_Usuario" name="id_Usuario" type="hidden"
                                        value="<?php echo $_GET["id_Usuario"]; ?>">
                                    <?php
                        } else {
                        ?>
                                    <input id="id_Usuario" name="id_Usuario" type="hidden">
                                    <?php
                        }
                        ?>

                        <div class=" col-5 offset-1 ">

                            <label for="formGroupExampleInput2" class="form-label">Nombre
                            </label><label class="text-danger">*</label>
                            <input type="text" id="Nombre" name="Nombre" class="form-control mb-3 w-75 shadow-sm" placeholder="Nombre"
                                aria-label="Nombre" aria-describedby="basic-addon2">

                            <label for="formGroupExampleInput2" class="form-label">Apellidos</label>
                            <label class="text-danger">*</label>
                            <input type="text" id="Apellidos" name="Apellidos" class="form-control mb-4 w-75" placeholder="Apellidos"
                                aria-label="Apellidos" aria-describedby="basic-addon2">

                            <select id="Tipo_de_documento" name="Tipo_de_documento" class="form-select mb-4 w-75 shadow-sm " aria-label="Default select example">
                                <option selected>Tipo de documento</option>
                                <option value="1">Cedula de Ciudadania</option>
                                <option value="2">Extrangeria</option>
                                <option value="3">Pasaporte</option>
                            </select>

                            <label for="formGroupExampleInput2" class="form-label ms-1">Numero de documento</label>
                            <label class="text-danger">*</label>
                            <input type="text" id="Numero_de_documento" name="Numero_de_documento" class="form-control mb-3 w-75  shadow-sm"
                                placeholder="Numero de documento" aria-label="Numero de documento"
                                aria-describedby="basic-addon2" style="width: 40%;">

                            <label for="formGroupExampleInput2" class="form-label ms-1">Telefono</label>
                            <label class="text-danger">*</label>
                            <input type="text" id="Telefono" name="Telefono" class="form-control mb-3 w-75  shadow-sm" placeholder="Telefono"
                                aria-label="Telefono" aria-describedby="basic-addon2">
                        </div>

                        <div class="col-5 offset-1 ">

                            <label for="formGroupExampleInput2" class="form-label">Direccion de residencia</label>
                            <label class="text-danger">*</label>
                            <input type="text" id="Direccion_de_residencia" name="Direccion_de_residencia" class="form-control mb-3 w-75 shadow-sm" placeholder="Direccion"
                                aria-label="Nombre" aria-describedby="basic-addon2">

                            <label for="formGroupExampleInput2" class="form-label">Correo electronico</label>
                            <label class="text-danger">*</label>
                            <input type="text" id="Correo_electronico" name="Correo_electronico" class="form-control mb-4 w-75 shadow-sm" placeholder="Correo electronico"
                                aria-label="Correo electronico" aria-describedby="basic-addon2">

                            <select id="id_rol" name="id_rol" class="form-select mb-4 w-75 shadow-sm" aria-label="Default select example">
                                <option selected>Tipo de usuario</option>
                                <option value="1">Administrador</option>
                                <option value="2">Colaborador</option>

                            </select>

                            <label for="formGroupExampleInput2" class="form-label">Nombre de usuario</label>
                            <label class="text-danger">*</label>
                            <input id="Usuario" name="Usuario" type="text" class="form-control mb-3 w-75 shadow-sm" placeholder="Nombre de usuario"
                                aria-label="Numero de documento" aria-describedby="basic-addon2">

                            <label for="formGroupExampleInput2" class="form-label">Clave de usuario</label>
                            <label class="text-danger">*</label>
                            <input  id="Contrasena" name="Contrasena" type="text" class="form-control mb-3 w-75 shadow-sm" placeholder="Clave de usuario"
                                aria-label="Telefono" aria-describedby="basic-addon2">

                        </div>

                        <div class="row col-12 my-3">
                            <div class="col-3 offset-3">
                                <button type="button" class="btn btn-danger ">Cancelar</button>
                            </div>
                            <div class="col-5 offset-1 ">
                                <button type="button" class="btn btn-primary" onclick="validate_data()">Guardar</button>
                            </div>
                        </div>

                        <div id="CajaPie" class="col-12">
                            <footer>
                                <!-- Copyright -->
                                <div class="py-4 text-center">
                                    <a class="text-dark text-decoration-none" href="">© 2020 Copyright</a>
                                    <img width="40px" src="../recursos/avanzar.png" alt="">
                                </div>
                            </footer>
                        </div>
                    </div>
                </form>
            </div>

            <!--Final Contenido-->

        </div>
    </div>
    <script src="./form.js"></script>
</body>

</html>