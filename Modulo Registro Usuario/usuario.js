const URL_SERVER_USUARIO = "http://localhost/avanzacompany/backend/Api/controllers/usuario.php?method=get_all";

$(document).ready(() => {
    load_data();
});

const load_data = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_USUARIO
    })
    .done(function(data) {
        console.log(data);
        data = JSON.parse(data);
        if(data.status === "success") {
            for(usuario of data.usuario) {
                $("#container_data").append(`
                    <div class="d-flex col-12">
                        <section class="d-flex column col-12 pt-2 text-center  text-dark fs-7 ">
                            <div style="margin:1%; width: 100px; ">${usuario.Nombre}</div>
                            <div style="margin:1%; width: 100px; ">${usuario.Apellidos}</div>
                            <div style="margin:1%; width: 100px; ">${usuario.Tipo_de_documento}</div>
                            <div style="margin:1%; width: 100px; ">${usuario.Numero_de_documento}</div>
                            <div style="margin:1%; width: 100px; ">${usuario.Usuario}</div>
                            <div style="margin:1%; width: 100px; ">${usuario.Contrasena}</div>
                            <div style="margin:1%; width: 100px; ">${usuario.Telefono}</div>
                            <div style="margin:1%; width: 100px; ">
                            <a class="botonUsuario" href="./editar.php?id_Usuario=${usuario.id_Usuario}" alt="">
                            <button style="width: 40px; height: 30px; background-color:#33A2FF;" alt="editar"><i class="bi bi-pencil-square"></i></button>
                            </a>
                            <button class="botonUsuario" style="width: 40px; height: 30px; background-color:#FF334C;" alt="eliminar"><i class="bi bi-trash"></i></button>
                            </div>
                        </section>
                    </div>
                `);
            }
        }
    });
}