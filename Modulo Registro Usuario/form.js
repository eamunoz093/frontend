const URL_SERVER = "http://localhost/avanzacompany/backend/Api/";
const URL_SERVER_SAVE = `${URL_SERVER}/controllers/usuario.php?method=create`;
const URL_SERVER_UPDATE = `${URL_SERVER}/controllers/usuario.php?method=update`;
const URL_SERVER_GET = `${URL_SERVER}/controllers/usuario.php?method=get_by_id`;
$(document).ready(() => {
    if(!$("#id_Usuario").val() == '') {
        get_data_from_server();
    }
});

const validate_data = () => {
    if($("#id_Usuario").val() == '') {
        save_data();
    } else {
        update_data();
    }
}

const get_data_from_server = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_GET,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            document.formData.id_rol.value = data.usuario.id_rol;
            document.formData.Nombre.value = data.usuario.Nombre;
            document.formData.Apellidos.value = data.usuario.Apellidos;
            document.formData.Tipo_de_documento.value = data.usuario.Tipo_de_documento;
            document.formData.Numero_de_documento.value = data.usuario.Numero_de_documento;
            document.formData.Telefono.value = data.usuario.Telefono;
            document.formData.Direccion_de_residencia.value = data.usuario.Direccion_de_residencia;
            document.formData.Correo_electronico.value = data.usuario.Correo_electronico;
            document.formData.Usuario.value = data.usuario.Usuario;
            document.formData.Contrasena.value = data.usuario.Contrasena;
            
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const get_data_from_form = () => {
    let data = {
        "id_Usuario": document.formData.id_Usuario.value,
        "id_rol": document.formData.id_rol.value,
        "Nombre": document.formData.Nombre.value,
        "Apellidos": document.formData.Apellidos.value,
        "Tipo_de_documento": document.formData.Tipo_de_documento.value,
        "Numero_de_documento": document.formData.Numero_de_documento.value,
        "Telefono": document.formData.Telefono.value,
        "Direccion_de_residencia": document.formData.Direccion_de_residencia.value,
        "Correo_electronico": document.formData.Correo_electronico.value,
        "Usuario": document.formData.Usuario.value,
        "Contrasena": document.formData.Contrasena.value,
        

    }

    return data;
}

const save_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_SAVE,
        data: get_data_from_form()
    })
    .done(function(data) {
        console.log(data);
        data = JSON.parse(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Registro Usuario/Vista_Usuario.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const update_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_UPDATE,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        console.log(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Registro Usuario/Vista_Usuario.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}