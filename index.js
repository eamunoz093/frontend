const URL_SERVER = "http://localhost/avanzacompany/backend/Api/";
const URL_SERVER_LOGIN = `${URL_SERVER}/controllers/users.php?method=login`;
const Login = () => {
    let userLogin ={
        "user": "",
        "pass": ""
    }
    userLogin.user = document.formLogin.username.value
    userLogin.pass = document.formLogin.password.value
    if(userLogin.user == '' || userLogin.pass == '')
        alert("Los campos son obligatorios")
    else
        validar(userLogin)
}

const validar = userLogin => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_LOGIN,
        data: userLogin
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.login === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Registro Usuario/Vista_Usuario.html"
        } else {
            alert("Usuario o contraseña invalido");
        }
    });
}

//funcion mostrar contraseña//
function mostrarPassword(){
    var cambio = document.getElementById("password");
    if(cambio.type == "password"){
        cambio.type = "text";
        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
    }else{
        cambio.type = "password";
        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
    }
} 




