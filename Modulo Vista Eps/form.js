const URL_SERVER = "http://localhost/avanzacompany/backend/Api/";
const URL_SERVER_SAVE = `${URL_SERVER}/controllers/eps.php?method=create`;
const URL_SERVER_UPDATE = `${URL_SERVER}/controllers/eps.php?method=update`;
const URL_SERVER_GET = `${URL_SERVER}/controllers/eps.php?method=get_by_id`;
$(document).ready(() => {
    if(!$("#id_eps").val() == '') {
        get_data_from_server();
    }
});

const validate_data = () => {
    if($("#id_eps").val() == '') {
        save_data();
    } else {
        update_data();
    }
}

const get_data_from_server = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_GET,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            document.formData.Nombre_de_la_EPS.value = data.eps.Nombre_de_la_EPS;
            document.formData.NIT.value = data.eps.NIT;
            document.formData.telefono.value = data.eps.telefono;
            document.formData.Codigo.value = data.eps.Codigo;
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const get_data_from_form = () => {
    let data = {
        "id_eps": document.formData.id_eps.value,
        "Nombre_de_la_EPS": document.formData.Nombre_de_la_EPS.value,
        "NIT": document.formData.NIT.value,
        "telefono": document.formData.telefono.value,
        "Codigo": document.formData.Codigo.value
    }

    return data;
}

const save_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_SAVE,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Vista Eps/Vista_Eps.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const update_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_UPDATE,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        console.log(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Vista Eps/Vista_Eps.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}