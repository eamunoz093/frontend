const URL_SERVER = "http://localhost/avanzacompany/backend/Api/";
const URL_SERVER_SAVE = `${URL_SERVER}/controllers/cliente.php?method=create`;
const URL_SERVER_UPDATE = `${URL_SERVER}/controllers/cliente.php?method=update`;
const URL_SERVER_GET = `${URL_SERVER}/controllers/cliente.php?method=get_by_id`;
$(document).ready(() => {
    if(!$("#id_Cliente").val() == '') {
        get_data_from_server();
    }
});

const validate_data = () => {
    if($("#id_Cliente").val() == '') {
        save_data();
    } else {
        update_data();
    }
}

const get_data_from_server = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_GET,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            document.formData.id_Cliente.value = data.cliente.id_Cliente;
            document.formData.Nombre.value = data.cliente.Nombre;
            document.formData.Apellidos.value = data.cliente.Apellidos;
            document.formData.Tipo_de_Documento.value = data.cliente.Tipo_de_Documento;
            document.formData.Numero_de_documento.value = data.cliente.Numero_de_documento;
            document.formData.Lugar_de_expedicion.value = data.cliente.Lugar_de_expedicion;
            document.formData.Telefono.value = data.cliente.Telefono;
            document.formData.Direccion_de_residencia.value = data.cliente.Direccion_de_residencia;
            document.formData.Tipo_de_sangre.value = data.cliente.Tipo_de_sangre;
            document.formData.Beneficiarios.value = data.cliente.Beneficiarios;
            document.formData.id_plan.value = data.cliente.id_plan;
            document.formData.id_eps.value = data.cliente.id_eps;
            document.formData.id_arl.value = data.cliente.id_arl;
            document.formData.id_afp.value = data.cliente.id_afp;
            document.formData.id_caja.value = data.cliente.id_caja;
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const get_data_from_form = () => {
    let data = {
        "id_Cliente": document.formData.id_Cliente.value,
        "Nombre": document.formData.Nombre.value,
        "Apellidos": document.formData.Apellidos.value,
        "Tipo_de_Documento": document.formData.Tipo_de_Documento.value,
        "Numero_de_documento": document.formData.Numero_de_documento.value,
        "Lugar_de_expedicion": document.formData.Lugar_de_expedicion.value,
        "Telefono": document.formData.Telefono.value,
        "Direccion_de_residencia": document.formData.Direccion_de_residencia.value,
        "Tipo_de_sangre": document.formData.Tipo_de_sangre.value,
        "Beneficiarios": document.formData.Beneficiarios.value,
        "id_plan": document.formData.id_plan.value,
        "id_eps": document.formData.id_eps.value,
        "id_arl": document.formData.id_arl.value,
        "id_afp": document.formData.id_afp.value,
        "id_caja": document.formData.id_caja.value,
        

    }

    return data;
}

const save_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_SAVE,
        data: get_data_from_form()
    })
    .done(function(data) {
        console.log(data);
        data = JSON.parse(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo registro de cliente/Vista_Cliente.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const update_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_UPDATE,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        console.log(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo registro de cliente/Vista_Cliente.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}