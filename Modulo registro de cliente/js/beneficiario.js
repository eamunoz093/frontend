$(document).ready(() => {
    $('#flexCheckIndeterminate').click(() => {
        if ($(this).is(':checked')) {
            $("#campos_beneficiario").show();
        } else {
            $("#campos_beneficiario").hide();
        }
    });
});