const URL_SERVER_CLIENTE = "http://localhost/avanzacompany/backend/Api/controllers/cliente.php?method=get_all";

$(document).ready(() => {
    load_data();
});

const load_data = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_CLIENTE
    })
    .done(function(data) {
        console.log(data);
        data = JSON.parse(data);
        if(data.status === "success") {
            for(cliente of data.cliente) {
                $("#container_data").append(`
                    <div class="d-flex col-12">
                        <section class="d-flex column col-12 pt-2 text-center  text-dark fs-7 ">
                            <div style="margin:1%; width: 100px; ">${cliente.Nombre}</div>
                            <div style="margin:1%; width: 100px; ">${cliente.Apellidos}</div>
                            <div style="margin:1%; width: 100px; ">${cliente.Tipo_de_Documento}</div>
                            <div style="margin:1%; width: 100px; ">${cliente.Numero_de_documento}</div>
                            <div style="margin:1%; width: 100px; ">${cliente.Lugar_de_expedicion}</div>
                            <div style="margin:1%; width: 100px; ">${cliente.Telefono}</div>
                            <div style="margin:1%; width: 100px; ">${cliente.Direccion_de_residencia}</div>
                            <div style="margin:1%; width: 100px; ">
                            <a class="botonUsuario" href="./editar.php?id_Cliente=${cliente.id_Cliente}" alt="">
                            <button style="width: 40px; height: 30px; background-color:#33A2FF;" alt="editar"><i class="bi bi-pencil-square"></i></button>
                            </a>
                            <button class="botonUsuario" style="width: 40px; height: 30px; background-color:#FF334C;" alt="eliminar"><i class="bi bi-trash"></i></button>
                            </div>
                        </section>
                    </div>
                `);
            }
        }
    });
}