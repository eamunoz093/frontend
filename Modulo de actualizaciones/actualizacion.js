const URL_SERVER_EPS = "http://localhost/avanzacompany/backend/Api/controllers/eps.php?method=get_all";

$(document).ready(() => {
    load_data();
});

const load_data = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_EPS
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            for(eps of data.eps) {
                $("#container_data").append(`
                    <div class="d-flex col-12">
                        <section class="d-flex column col-12 pt-2 text-center  text-dark fs-7 ">
                            <div class=" col-3 p-1 border">${eps.Nombre_de_la_EPS}</div>
                            <div class=" col-3 p-1 border">${eps.NIT}</div>
                            <div class=" col-3 p-1 border">${eps.telefono}</div>
                            <div class=" col-2 p-1 border">${eps.Codigo}</div>
                            <div class=" col-1 p-1 border">
                            <a class="botonEps" href="./editar.php?id_eps=${eps.id_eps}" alt="">
                            <button style="width: 40px; height: 30px; background-color:#33A2FF;"><i class="bi bi-pencil-square"></i></button>
                            </a>
                            <button class="botonEps" style="width: 40px; height: 30px; background-color:#FF334C;" href="../recursos/delete.png" alt=""><i class="bi bi-trash"></i></button>
                            </div>
                        </section>
                    </div>
                `);
            }
        }
    });
}