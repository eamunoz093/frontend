const URL_SERVER = "http://localhost/avanzacompany/backend/Api/";
const URL_SERVER_SAVE = `${URL_SERVER}/controllers/caja.php?method=create`;
const URL_SERVER_UPDATE = `${URL_SERVER}/controllers/caja.php?method=update`;
const URL_SERVER_GET = `${URL_SERVER}/controllers/caja.php?method=get_by_id`;
$(document).ready(() => {
    if(!$("#id_caja").val() == '') {
        get_data_from_server();
    }
});

const validate_data = () => {
    if($("#id_caja").val() == '') {
        save_data();
    } else {
        update_data();
    }
}

const get_data_from_server = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_GET,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            document.formData.Nombre_de_la_Caja.value = data.caja.Nombre_de_la_Caja;
            document.formData.NIT.value = data.caja.NIT;
            document.formData.telefono.value = data.caja.telefono;
            document.formData.Codigo.value = data.caja.Codigo;
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const get_data_from_form = () => {
    let data = {
        "id_caja": document.formData.id_caja.value,
        "Nombre_de_la_Caja": document.formData.Nombre_de_la_Caja.value,
        "NIT": document.formData.NIT.value,
        "telefono": document.formData.telefono.value,
        "Codigo": document.formData.Codigo.value
    }

    return data;
}

const save_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_SAVE,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Vista Caja de compensacion/Vista_Caja_de_compensacion.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}

const update_data = () => {
    $.ajax({
        method: "POST",
        url: URL_SERVER_UPDATE,
        data: get_data_from_form()
    })
    .done(function(data) {
        data = JSON.parse(data);
        console.log(data);
        if(data.status === "success") {
            window.location = "http://localhost/avanzacompany/frontend/Modulo Vista Caja de compensacion/Vista_Caja_de_compensacion.html"
        } else {
            alert("Ha ocurrido un error contacte al administrador");
        }
    });
}
