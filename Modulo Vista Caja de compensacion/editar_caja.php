<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Avanzar company</title>
  <link rel="icon" href="../recursos/avanzar.ico">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/index-css/normalize.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="css/Fondo.css">
  <link rel="stylesheet" href="css/efecto_hover.css">
  <link rel="stylesheet" href="css/Nav_Superior y footer.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="../assets/js/jquery.js"></script>
</head>


<body>

  <div class="container-fluid">

    <div class="row">

      <!--Menu lateral izquierdo-->
      <div class="col-12 col-lg-3">
        <div style="width: 20%;" class="ms-5 list-group fixed-top">

          <a id="fondos" href="../Modulo Registro Usuario/registro_usuario.html"
            class="py-4 list-group-item list-group-item-action text-center fs-5 ">
            <i class="bi bi-person">Usuario </i></a>

            <a id="fondos" href="../Modulo registro de cliente/modulo-registro-de-cliente.html"
            class="py-4 list-group-item list-group-item-action text-center fs-5">
            <i class="bi bi-people-fill"> Cliente</i></a>

          <a id="fondos" href="../Modulo de actualizaciones/Actualizaciones.html"
            class="py-4 list-group-item list-group-item-action text-center fs-5">
            Consulta Cliente</a>

          <a id="fondos" href="../Modulo Vista Eps/Vista_Eps.html" 
          class="py-4 list-group-item list-group-item-action text-center fs-5">EPS</a>

          <a id="fondos" href="../Modulo Vista Arl/Vista_Arl.html" 
          class="py-4 list-group-item list-group-item-action text-center fs-5">ARL</a>

          <a id="fondos" href="../Modulo Vista Afp/Vista_Afp.html" 
          class="py-4 list-group-item list-group-item-action text-center fs-5">AFP</a>

          <a id="fondos" href="../Modulo Vista Caja de compensacion/Vista_Caja_de_compensacion.html" 
          class="py-4 list-group-item list-group-item-action text-center fs-5">Caja Compensacion</a>
        </div>
      </div>
      <!--Final del menu lateral-->

      <!--barra derecha superior-->

      <!--cuadro superior-->
      <div style="width: 950px; margin-left: 29%;" class="navegador col-12 bg-info shadow-sm fixed-top">
        <nav class="d-flex justify-content-center pt-4 navbar navbar-expand-lg navbar-light ">
          <div class=" ">
            <ul class="navbar-nav me-auto mb-2 my-6">
              <li class="nav-item ">
                <a class="navbar-text text-white text-decoration-none h4"><b>Bienvenido</b></a>
              </li>
              <li id="tipo_de_usuario" class="nav-item">
                <a class="navbar-text text-primary text-decoration-none h4"><b>Administrador</b></a>
              </li>
              
              <li class="nav-item">
                <a href="/frontend/index.html">
                  <button type="button" class=" boton btn-primary rounded">Exit</button>
                </a>
              </li>
            </ul>

          </div>
        </nav>
      </div>
      <!--Final del navegador superior-->

      <!--Contenido Registro de usuario-->

      <div style="position: absolute; top: 20%; left: 29%;  width: 900px;" class="col-12">

        <form class="row col-12" name="formData">
        <?php
          if(isset($_GET["id_caja"])){
        ?>
            <input id="id_caja" name="id_caja" type="hidden" value="<?php echo $_GET["id_caja"]; ?>">
        <?php
          } else {
        ?>
            <input id="id_caja" name="id_caja" type="hidden">
        <?php
          }
        ?>
          <h2  class="text-center p-4">Caja de Compesacion</h2>
          <div class=" col-5 offset-1 ">
            <label for="formGroupExampleInput2" class="form-label">Nombre
            </label><label class="text-danger">*</label>
            <input id="Nombre_de_la_Caja" name="Nombre_de_la_Caja" type="text" class="form-control mb-3 w-75 shadow-sm" placeholder="Nombre" aria-label="Nombre"
              aria-describedby="basic-addon2">

              <label for="formGroupExampleInput2" class="form-label">Nit</label>
            <label class="text-danger">*</label>
            <input id="NIT" name="NIT" type="text" class="form-control mb-4 w-75" placeholder="Nit" aria-label="Nit" aria-describedby="basic-addon2">
 
          </div>

          <div class="col-5 offset-1 ">

          <label for="formGroupExampleInput2" class="form-label">Telefono</label>
            <label class="text-danger">*</label>
            <input id="telefono" name="telefono" type="text" class="form-control mb-3 w-75 shadow-sm" placeholder="Telefono" aria-label="Nombre" aria-describedby="basic-addon2">

            <label for="formGroupExampleInput2" class="form-label">Codigo</label>
            <label class="text-danger">*</label>
            <input id="Codigo" name="Codigo" type="text" class="form-control mb-4 w-75 shadow-sm" placeholder="Codigo"
              aria-label="Codigo" aria-describedby="basic-addon2">

          </div>

          <div class="row col-12 my-3">
            <div class="col-3 offset-3">
              <button type="button" class="btn btn-danger " href="./Vista_Caja.html">Cancelar</button>
            </div>
            <div class="col-5 offset-1 ">
              <button type="button" class="btn btn-primary" onclick="validate_data()">Guardar</button>
            </div>
          </div>

          <div id="CajaPie" class="col-12">
            <footer>
              <!-- Copyright -->
              <div class="py-4 text-center">
                <a class="text-dark text-decoration-none" href="">© 2020 Copyright</a>
                <img width="40px" src="../recursos/avanzar.png" alt="">
              </div>
            </footer>
          </div>
        </form>
      </div>

      <!--Final Contenido-->

    </div>
  </div>
  
  <script src="./formC.js"></script>
</body>

</html>