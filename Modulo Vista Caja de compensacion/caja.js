const URL_SERVER_Caja = "http://localhost/avanzacompany/backend/Api/controllers/caja.php?method=get_all";

$(document).ready(() => {
    load_data();
});

const load_data = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_Caja
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            for(caja of data.caja) {
                $("#container_data").append(`
                <div class="d-flex col-12">
                    <section class="d-flex column col-12 pt-2 text-center  text-dark fs-7 ">
                        <div class=" col-3 p-1 border">${caja.Nombre_de_la_Caja}</div>
                        <div class=" col-3 p-1 border">${caja.NIT}</div>
                        <div class=" col-3 p-1 border">${caja.telefono}</div>
                        <div class=" col-2 p-1 border">${caja.Codigo}</div>
                        <div class=" col-1 p-1 border">
                        <a class="botonEps" href="./editar_caja.php?id_caja=${caja.id_caja}" alt="">
                        <button style="width: 40px; height: 30px; background-color:#33A2FF;" alt="editar">
                        <i class="bi bi-pencil-square"></i></button>
                        </a>
                        <button class="botonEps" style="width: 40px; height: 30px; background-color:#FF334C;" alt="eliminar">
                        <i class="bi bi-trash"></i></button>
                        </div>
                    </section>
                </div>
                `);
            }
        }
    });
}