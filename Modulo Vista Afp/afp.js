const URL_SERVER_AFP = "http://localhost/avanzacompany/backend/Api/controllers/afp.php?method=get_all";

$(document).ready(() => {
    load_data();
});

const load_data = () => {
    $.ajax({
        method: "GET",
        url: URL_SERVER_AFP
    })
    .done(function(data) {
        data = JSON.parse(data);
        if(data.status === "success") {
            for(afp of data.afp) {
                $("#container_data").append(`
                <div class="d-flex col-12">
                    <section class="d-flex column col-12 pt-2 text-center  text-dark fs-7 ">
                        <div class=" col-3 p-1 border">${afp.Nombre_de_la_AFP}</div>
                        <div class=" col-3 p-1 border">${afp.NIT}</div>
                        <div class=" col-3 p-1 border">${afp.telefono}</div>
                        <div class=" col-2 p-1 border">${afp.Codigo}</div>
                        <div class=" col-1 p-1 border">
                        <a class="botonEps" href="./editar_afp.php?id_afp=${afp.id_afp}" alt="">
                        <button style="width: 40px; height: 30px; background-color:#33A2FF;"><i class="bi bi-pencil-square"></i></button>
                        </a>
                        <button class="botonEps" style="width: 40px; height: 30px; background-color:#FF334C;" alt="Eliminar"><i class="bi bi-trash"></i></button>
                        </div>
                    </section>
                    </div>
                `);
                
            }
        }
    });
}